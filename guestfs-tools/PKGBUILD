# Maintainer: David Runge <dvzrv@archlinux.org>

pkgname=guestfs-tools
pkgver=1.52.0
pkgrel=1
pkgdesc="Tools for accessing and modifying guest disk images"
arch=(x86_64)
url="http://libguestfs.org/"
license=('GPL-2.0-or-later AND LGPL-2.1-or-later')
depends=(
  gcc-libs
  glib2 libglib-2.0.so libgobject-2.0.so
  glibc
  jansson
  libguestfs libguestfs.so
  libosinfo
  libvirt libvirt.so
  libxcrypt libcrypt.so
  libxml2 libxml2.so
  ncurses libncursesw.so
  pcre2 libpcre2-8.so
  perl-libintl-perl
  xz liblzma.so
)
makedepends=(bash-completion libisoburn ocaml ocaml-findlib perl-module-build)
backup=(
  etc/virt-builder/repos.d/libguestfs.conf
  etc/virt-builder/repos.d/libguestfs.gpg
  etc/virt-builder/repos.d/opensuse.conf
  etc/virt-builder/repos.d/opensuse.gpg
)
source=(
  "http://download.libguestfs.org/$pkgname/${pkgver%.*}-stable/$pkgname-$pkgver.tar.gz"{,.sig}
  "$pkgname"-tests-fix.patch::https://github.com/libguestfs/guestfs-tools/commit/a706f6f.patch
)
validpgpkeys=(F7774FB1AD074A7E8C8767EA91738F73E1B768A0) # Richard W.M. Jones <rjones@redhat.com>
sha256sums=('22fd132297045f90a67406f0ff0eee0f2a0106a5f1c8dcfc5c396a625ff78376'
            'SKIP'
            'fb66538205a8279467d849f1386717b9b64500e74c3739a7b79e1f481c8a3e96')

prepare() {
  patch -d $pkgname-$pkgver -Np1 -i ../$pkgname-tests-fix.patch
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr --sysconfdir=/etc
  make
}

check() {
  #
  # the test suite is slow AF unless /dev/kvm is available inside the nspawn
  # container e.g. pass "-D /dev/kvm" to makechrootpkg
  #
  # 1 test will be skipped if unable to access /dev/fuse
  #
  # FIXME Oct '23 passing "-D /dev/kvm" no longer works after:
  # https://gitlab.archlinux.org/archlinux/devtools/-/merge_requests/172
  #
  # If the following MR is accepted:
  # https://gitlab.archlinux.org/archlinux/devtools/-/merge_requests/201
  # Pass this to makechrootpkg:
  # -D /dev/kvm -D /dev/fuse -s --keep-unit

  cd $pkgname-$pkgver
  make check
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
}
