# Maintainer: Brett Cornwall <ainola@archlinux.org>
# Contributor: Balló György <ballogyor+arch at gmail dot com>
# Contributor: Sergej Pupykin <pupykin.s+arch@gmail.com>
# Contributor: Stefano Facchini <stefano.facchini@gmail.com>
# Contributor: Jonathan Lestrelin <zanko@daemontux.org>
# Contributor: Lucio Zara <pennega@gmail.com>

pkgname=spice-gtk
pkgver=0.42
pkgrel=1
pkgdesc="GTK+ client library for SPICE"
arch=(x86_64)
url="https://www.spice-space.org/"
license=(LGPL-2.1-or-later)
depends=(
  acl libacl.so
  cairo libcairo.so
  gdk-pixbuf2 libgdk_pixbuf-2.0.so
  glib2 libgio-2.0.so libglib-2.0.so libgobject-2.0.so
  glibc
  gst-plugins-base-libs
  gstreamer
  gtk3 libgdk-3.so libgtk-3.so
  json-glib libjson-glib-1.0.so
  libcacard libcacard.so
  libcap-ng libcap-ng.so
  libepoxy libepoxy.so
  libjpeg libjpeg.so
  libsasl libsasl2.so
  libsoup3 libsoup-3.0.so
  libusb libusb-1.0.so
  libx11
  lz4
  openssl libcrypto.so libssl.so
  opus libopus.so
  phodav
  pixman libpixman-1.so
  polkit libpolkit-gobject-1.so
  usbredir libusbredirhost.so libusbredirparser.so
  wayland libwayland-client.so libwayland-server.so
  zlib libz.so
)
makedepends=(
  gobject-introspection
  meson
  python-pyparsing
  python-six
  spice-protocol
  usbutils
  vala
  wayland-protocols
)
optdepends=(
  'gst-plugins-good: GStreamer video decoding support - jpegdec, vp8dec, vp9dec + GStreamer audio backend'
  'gst-plugins-bad: GStreamer video decoding support - h264parse, h265parse'
  'gst-libav: GStreamer video decoding support - avdec_h264, avdec_h265'
)
provides=(
  spice-glib="$pkgver"
  spice-gtk3="$pkgver"
  libspice-client-glib-2.0.so
  libspice-client-gtk-3.0.so
)
replaces=(spice-glib spice-gtk3)
source=("${url}download/gtk/$pkgname-$pkgver.tar.xz"{,.sig})
validpgpkeys=(206D3B352F566F3B0E6572E997D9123DE37A484F) # Victor Toso de Carvalho <me@victortoso.com>
sha256sums=('9380117f1811ad1faa1812cb6602479b6290d4a0d8cc442d44427f7f6c0e7a58'
            'SKIP')

build() {
  arch-meson $pkgname-$pkgver build -D usb-acl-helper-dir=/usr/lib/spice-gtk
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  meson install -C build --destdir "$pkgdir"
}
