# pkgbuilds

Just a bunch of personal PKGBUILDs for [Arch Linux](https://archlinux.org)
focused on my areas of interest.

Because Arch follows a rolling-release model, it's pretty much always
up-to-date...except when it's not. Thankfully, Arch provides the [Arch Build
System][abs] which allows you to take matters into your own hands.

These PKGBUILDs are based on upstream Arch, but have been modified for:

- testing
- making fixes
- optimization
- preferred style
- fun and games :smiley:

Bugs are [reported upstream][bugs] as appropriate.

## Standards

Say what?

- adhere to the [Arch package guidelines](https://wiki.archlinux.org/title/Arch_package_guidelines) as closely as possible
- packages are **only** built [in a clean chroot environment](https://wiki.archlinux.org/title/DeveloperWiki:Building_in_a_clean_chroot)
- aim for lint-free [ShellCheck](https://www.shellcheck.net) output (with exceptions, see [below](#shellcheck))
- always run the package test suite if feasible
- pay attention to [Namcap](https://wiki.archlinux.org/title/namcap) output (but ignore the obviously bogus warnings)

## Style

Mostly matters of personal taste:

- minimal variable quoting\*
- no excess use of curly braces around variables\*
- no quoting of individual array elements (except for optdepends)
- sort array elements alphabetically
- if length of array exceeds 1 line, put individual elements on their own line
- use arrays for argument lists when it makes sense, especially for ease of inserting comments
- remove redundant command-line flags, or at least annotate them if they are the default
- be verbose with cp, install, mv, rm, etc.

> \*Although Bash best practice is to quote and brace-delimit variables, I
> prefer to keep things clean and simple in the confines of an Arch PKGBUILD.

## ShellCheck

Because PKGBUILDs are essentially shell fragments and not fully self-contained
bash scripts, ShellCheck is run with some necessary exclusions:

    shellcheck -s bash -e SC2034,SC2154,SC2164 PKGBUILD

- <https://github.com/koalaman/shellcheck/wiki/SC2034>
- <https://github.com/koalaman/shellcheck/wiki/SC2154>
- <https://github.com/koalaman/shellcheck/wiki/SC2164>

## My Setup

I use a [custom local repository][clr] and place the /etc/pacman.conf directive
above the standard repos to ensure it [takes precedence][repos].

The [aurutils](https://github.com/AladW/aurutils) are employed to help manage
the custom local repo. For example:

    aur build -c -d custom -D "$CHROOT" -N --pacman-conf=myfile --makepkg-conf=myfile

[abs]: https://wiki.archlinux.org/title/Arch_Build_System
[bugs]: https://bugs.archlinux.org/index.php?string=&project=0&status[]=&opened=Toolybird
[clr]: https://wiki.archlinux.org/title/Pacman/Tips_and_tricks#Custom_local_repository
[repos]: https://man.archlinux.org/man/pacman.conf.5.en#REPOSITORY_SECTIONS
